using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private string staticScene = "StaticScene";
    [SerializeField] private string mainMenuScene = "MainMenuScene";
    [SerializeField] private string gameScene = "GameScene";

    private static SceneLoader instance;
    public static SceneLoader Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
        LoadMainMenu();
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadSceneAsync(mainMenuScene, LoadSceneMode.Additive);
    }

    public void RemoveMainMenu()
    {
        SceneManager.UnloadSceneAsync(mainMenuScene);
    }

    public void LoadGameScene()
    {
        SceneManager.LoadSceneAsync(gameScene, LoadSceneMode.Additive);

    }
    public void RemoveGameScene()
    {
        SceneManager.UnloadSceneAsync(gameScene);
    }
}

