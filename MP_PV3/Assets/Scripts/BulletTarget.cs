using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class BulletTarget : MonoBehaviour
{
    private int nVidasRestadas = 1;
    [SerializeField] Transform vfxMuerte;
    [SerializeField] AudioSource explosionSound;

    private void OnTriggerEnter (Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            other.GetComponent<SistemaVidas>().RestarVidas(nVidasRestadas);
        }
        Instantiate(vfxMuerte, transform.position, Quaternion.identity);
        //explosionSound.Play();
        Destroy(gameObject);
    }
}
