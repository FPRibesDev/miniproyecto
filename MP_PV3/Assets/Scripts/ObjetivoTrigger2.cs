using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetivoTrigger2 : MonoBehaviour
{
    int i = 2;
    [SerializeField] GameObject objetiveTracker;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            objetiveTracker.GetComponent<Objetivos>().CambiarObjetivo(i);
        }
    }
}
