using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetivoTrigger3 : MonoBehaviour
{
    int i = 3;
    [SerializeField] GameObject objetiveTracker;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            objetiveTracker.GetComponent<Objetivos>().CambiarObjetivo(i);
        }
    }
}
