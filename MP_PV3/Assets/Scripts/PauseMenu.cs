using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    [SerializeField] string currentScene;
    [SerializeField] string nextScene;
    [SerializeField] GameObject Alarm;
    [SerializeField] GameObject electric;
    [SerializeField] GameObject estatico;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(GameIsPaused == false)
            {
                Debug.Log("pausado");
                Pause();
            }
            else
            {
                Debug.Log("resumido");
                Resume();
            }
        }
    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        Alarm.GetComponent<AudioSource>().enabled = false;
        electric.GetComponent<AudioSource>().enabled = false;
        estatico.GetComponent<AudioSource>().enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        GameIsPaused = true;
    }
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        Alarm.GetComponent<AudioSource>().enabled = true;
        electric.GetComponent<AudioSource>().enabled = true;
        estatico.GetComponent<AudioSource>().enabled = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameIsPaused = false;
    }
    public void RegresarMenuPrincipal()
    {
        pauseMenuUI.SetActive(false);
        SceneManager.UnloadSceneAsync(currentScene);
        SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Additive);
    }
}
