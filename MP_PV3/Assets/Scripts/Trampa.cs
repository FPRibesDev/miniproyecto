using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampa : MonoBehaviour
{
    public GameObject trampa;
    public GameObject player;
    private int nVidasRestadas = 1;
    [SerializeField] Transform vfxHit;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            Debug.Log("Pillado!");
            other.GetComponent<SistemaVidas>().RestarVidas(nVidasRestadas);
            Instantiate(vfxHit, transform.position, Quaternion.identity);
        }
    }
}
