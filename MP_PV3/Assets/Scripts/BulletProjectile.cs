using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    private Rigidbody bulletRigidbody;
    private void Awake()
    {
        bulletRigidbody = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        float speed = 50f;
        bulletRigidbody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        /*if(other.GetComponent<BulletTarget>() != null)
        {
            Debug.Log("Hit");
            Destroy(gameObject);
        }
        else if(other.CompareTag("Trigger"))
        {

        }
        else
        {
            Debug.Log("Miss");
            Destroy(gameObject);
        }*/
        if(other.GetComponent<MinaTrigger>() != null)
        {
            Debug.Log("Atravesando trigger");
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
