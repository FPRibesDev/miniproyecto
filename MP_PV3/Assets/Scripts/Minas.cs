using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minas : MonoBehaviour
{
    //public GameObject[] waypoints;
    [SerializeField] GameObject waypoint1;
    [SerializeField] GameObject waypoint2;
    [SerializeField] GameObject playerWaypoint;
    GameObject oldWaypoint;
    int estado;
    //int current = 0;
    //float rotSpeed;
    public float speed;
    float WPradius = 1;
    float distance = 10;
    bool insideRange;
    GameObject currentWaypoint;
    private void Awake()
    {
        currentWaypoint = waypoint1;
        estado = 0;
        oldWaypoint = currentWaypoint;
        insideRange = false;
    }
    private void Update()
    {
       

        if (Vector3.Distance(currentWaypoint.transform.position, transform.position) > WPradius)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentWaypoint.transform.position, Time.deltaTime * speed);
        }
        else
        {
           if(currentWaypoint == waypoint1)
            {
                currentWaypoint = waypoint2;
            }
            else if (currentWaypoint == waypoint2)
            {
                currentWaypoint = waypoint1;
            }
            else
            {
                currentWaypoint = oldWaypoint;
            }
        }
        
        if (Vector3.Distance(playerWaypoint.transform.position, transform.position) < distance)
        {
            if (!insideRange)
            {
                insideRange = true;
                oldWaypoint = currentWaypoint;
                currentWaypoint = playerWaypoint;
            }
           
        }
        else
        {
            if (insideRange)
            {
                insideRange = false;
                currentWaypoint = oldWaypoint;
            }
        }
    }
    /*public void SetEstado(int nuevoEstado)
    {
        if(nuevoEstado == 0)
        {
            Debug.Log("Es 0");
            currentWaypoint = oldWaypoint;
        }
        else
        {
            Debug.Log("Es 1");
            oldWaypoint = currentWaypoint;
            currentWaypoint = playerWaypoint;
        }
    }*/
}
