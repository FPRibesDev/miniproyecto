using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objetivos : MonoBehaviour
{
    [SerializeField] Text objetivoTxt;
    void Start()
    {
        objetivoTxt.text = "Objetive: Escape the Hangar";
    }
    public void CambiarObjetivo(int i)
    {
        if(i == 1)
        {
            objetivoTxt.text = "Objetive: Escape the Dorms";
        }
        else if(i == 2)
        {
            objetivoTxt.text = "Objetive: Escape the Storageroom";
        }
        else if(i == 3)
        {
            objetivoTxt.text = "Objetive: Access the Bridge";
        }
        else
        {
            objetivoTxt.text = "No objetive remaining";
        }
    }
}
