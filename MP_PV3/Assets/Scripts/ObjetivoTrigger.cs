using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjetivoTrigger : MonoBehaviour
{
    int i = 1;
    [SerializeField] GameObject objetiveTracker;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<ThirdPersonShooterController>() != null)
        {
            objetiveTracker.GetComponent<Objetivos>().CambiarObjetivo(i);
        }
    }
}
