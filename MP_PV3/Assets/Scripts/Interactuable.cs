using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactuable : MonoBehaviour
{
    bool hasBeenAvtivated = false;
    public GameObject puerta;
    [SerializeField] Transform vfxActivation;
    [SerializeField] AudioSource portalActivacion;


    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<ThirdPersonShooterController>() != null && hasBeenAvtivated == false)
        {
            //Instantiate(vfxActivation, transform.position, Quaternion.identity);
            portalActivacion.Play();
            puerta.SetActive(false);
            hasBeenAvtivated = true;
        }
    }
}
