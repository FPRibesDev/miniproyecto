using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SistemaVidas : MonoBehaviour
{
    public int vidasInicial = 3;
    int vidasActuales;
    public Text contadorVidas;
    [SerializeField] GameObject PantallaMuerte;
    [SerializeField] AudioSource damageSound;
    [SerializeField] AudioSource deadSound;
    [SerializeField] AudioSource alarmSound;
    public void Awake()
    {
        vidasActuales = vidasInicial;
        MostrarVidas();
    }
    public void RestarVidas(int nVidasRestadas)
    {
        vidasActuales = vidasActuales - nVidasRestadas;
        MostrarVidas();
        if(vidasActuales > 0)
        {
            damageSound.Play();
        }

        if(vidasActuales <= 0)
        {
            alarmSound.GetComponent<AudioSource>().Stop();
            deadSound.Play();
            Morir();
        }
    }
    public void Morir()
    {
        Debug.Log("Has muerto");
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PantallaMuerte.SetActive(true);
    }
    public void SumarVidas(int sumarVidas)
    {
        vidasActuales += sumarVidas;
        MostrarVidas();
    }

    private void MostrarVidas()
    {
        contadorVidas.text = $"Lives: {vidasActuales}";
    }
}
