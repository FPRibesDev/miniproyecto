using Cinemachine;
using StarterAssets;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ThirdPersonShooterController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform debugTransform;
    [SerializeField] private Transform pfBulletProjectile;
    [SerializeField] private Transform spawnBulletPosition;
    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private Animator animator;
    public int ammo;
    bool normalGun = true;
    public float gunHeat;
    public Text contador;
    public Text arma;
    [SerializeField] GameObject pistola;
    [SerializeField] AudioSource pistolShot;
    [SerializeField] AudioSource holsterPistol;
    [SerializeField] GameObject playerExplosion;
    [SerializeField] AudioSource noAmmo;
    int ammoToShoot;

    private void Awake()
    {
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        animator = GetComponent<Animator>();
        contador.text = ammo.ToString() + " AMMO";
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        gunHeat = 0.25f;
        arma.text = "PISTOL";
        ammoToShoot = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            normalGun = !normalGun;
            arma.text = normalGun ? "PISTOL" : "BURST";
            ammoToShoot = normalGun ? 1 : Mathf.Min(3, ammo);

        }

        Vector3 mouseWorldPosition = Vector3.zero;
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            debugTransform.position = raycastHit.point;
            mouseWorldPosition = raycastHit.point;
        }
        if (starterAssetsInputs.aim)
        {
            pistola.SetActive(true);
            aimVirtualCamera.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotateOnMove(false);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 1f, Time.deltaTime * 10f));

            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;

            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);
        }
        else
        {
            holsterPistol.Play();
            pistola.SetActive(false);
            aimVirtualCamera.gameObject.SetActive(false);
            thirdPersonController.SetSensitivity(normalSensitivity);
            thirdPersonController.SetRotateOnMove(true);
            animator.SetLayerWeight(1, Mathf.Lerp(animator.GetLayerWeight(1), 0f, Time.deltaTime * 10f));
        }
        if (gunHeat > 0)
        {
            gunHeat -= Time.deltaTime;
        }
        if (starterAssetsInputs.aim)
        {
            if (starterAssetsInputs.shoot)
            {
                if (ammo > 0)
                 {
                    if (gunHeat <= 0)
                    {
                        Vector3 aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
                        Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
                        ammoToShoot--;
                        ammo--;
                        if(ammoToShoot == 0)
                        {
                            starterAssetsInputs.shoot = false;
                            ammoToShoot = normalGun ? 1 : ammoToShoot = Mathf.Min(3, ammo);
                        }
                        contador.text = ammo.ToString() + " AMMO";
                        pistolShot.Play();
                        gunHeat = 0.25f;
                    }
                }
            }
        }
        else
        {
            starterAssetsInputs.shoot = false;
        }
    }
    public void SumarMunicion (int municionRecibida)
    {
        ammo += municionRecibida;
        ammoToShoot = normalGun ? 1 : ammoToShoot = Mathf.Min(3, ammo);
        contador.text = ammo.ToString() + " AMMO";
    }
}
