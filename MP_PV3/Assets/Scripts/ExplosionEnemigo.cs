using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEnemigo : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemigo"))
        {
            Destroy(other);
        }
    }
}
