using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaqueteVida : MonoBehaviour
{
    public int vidaRecibida = 1;
    [SerializeField] Transform vfxGanarVida;
    [SerializeField] AudioSource healSound;
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            other.GetComponent<SistemaVidas>().SumarVidas(vidaRecibida);
            Instantiate(vfxGanarVida, transform.position, Quaternion.identity);
            healSound.Play();
            Destroy(gameObject);
        }

    }
}
