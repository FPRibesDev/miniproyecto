using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaqueteMunicion : MonoBehaviour
{
    public int municionRecibida;
    [SerializeField] Transform vfxGanarMunicion;
    [SerializeField] AudioSource ammoSound;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ThirdPersonShooterController>() != null)
        {
            other.GetComponent<ThirdPersonShooterController>().SumarMunicion(municionRecibida);
            Instantiate(vfxGanarMunicion, transform.position, Quaternion.identity);
            ammoSound.Play();
            Destroy(gameObject);
        }
    }
}
