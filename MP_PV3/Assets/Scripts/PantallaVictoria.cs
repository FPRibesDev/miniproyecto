using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PantallaVictoria : MonoBehaviour
{
    [SerializeField] GameObject pantallaVictoria;
    [SerializeField] AudioSource victorySound;
    [SerializeField] AudioSource staticSound;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<ThirdPersonShooterController>() != null)
        {
            pantallaVictoria.SetActive(true);
            Time.timeScale = 0f;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            victorySound.Play();
            staticSound.Stop();
        }
    }
}
