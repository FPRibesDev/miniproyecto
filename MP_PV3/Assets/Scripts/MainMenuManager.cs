using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] string currentScene;
    [SerializeField] string nextScene;
    [SerializeField] GameObject guia;
    public void Awake()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    public void LoadGameScene()
    {
        Debug.Log("Comenzando game scene");
        Time.timeScale = 1f;
        SceneLoader.Instance.LoadGameScene();
        SceneLoader.Instance.RemoveMainMenu();
    }
    public void SalirGame()
    {
        Debug.Log("Saliendo del juego");
        Application.Quit();
    }
    public void RegresarMainMenu()
    {
        Debug.Log("Regreso main menu");
        SceneManager.UnloadSceneAsync(currentScene);
        SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Additive);
    }
    IEnumerator LoadSceneWithDelay()
    {
        yield return new WaitForSeconds(0.2f);
        LoadGameScene();
    }
    public void LoadGameSceneButton()
    {
        StartCoroutine(LoadSceneWithDelay());
    }
    public void AbrirGuia()
    {
        guia.SetActive(true);
    }
    public void CerrarGuia()
    {
        guia.SetActive(false);
    }
}
